% !TeX spellcheck = en_US
\chapter{Fixed-point Arithmetic}
\label{sec:fixedpoint}
This appendix gives a short introduction into fixed-point arithmetic. Floating-point and fixed-point numbers were introduced in \cref{sec:CC3220S}.
The Q$n.m$ notation for fixed-point numbers was also explained in \cref{sec:CC3220S}.
In this appendix we will see how arithmetic operations (add, subtract, multiply, and divide) with fixed-point numbers can be performed.
As already explained the processor which we use is optimized for 16-bit fixed-point numbers.
In this appendix we will use 8-bit fixed-point numbers instead.
A Q$n,m$ fixed-point value will be stored in a $1+n+m$-bit two's complement integer variable. 

\section{Add and Subtract}
If we want to add or subtract two fixed-point numbers we have to align their radix points.
For example if $a$ is a Q5.2 encoded fixed point number with value 00001101 ($000011.01_2 = 3.25_{10}$) and $b$ is a Q4.3 encoded fixed point number with value 00100101 ($00100.101_2 = 4.625_{10}$), then $a + b$ can not be calculated by simply adding their values.
The radix points must be aligned before the addition as shown in \cref{fig:add}.
As can be seen in \cref{fig:add}, the result is a Q5.3 fixed-point number with value 000111110 ($000111.111_2 = 7.875_{10}$).

\begin{myFigure}
\centering%
\begin{BVerbatim}
 000011.01  +
  00100.101 =
 000111.111 
\end{BVerbatim}
\caption{Adding a Q4.3 to a Q5.2 fixed-point number.}
\label{fig:add}
\end{myFigure}   

In general, when we add a Q$n_1,m_1$ fixed-point number by a Q$n_2,m_2$ fixed-point number, the result will be a Q$\max(n_1+n_2+1),\max(m_1+m_2)$ fixed-point number.

When programming the number of bits we use for variables is most of the times fixed (e.g. to 8-bit).
In this case we can convert $a$ from Q5.2 to Q4.3 by shifting it one place to the left.
Care must be taken, not to generate an overflow by this operation.
After the shift we can use an integer addition because both numbers are Q4.3 and the result will also be Q4.3 (when we assume that no overflow occurs when performing the integer addition).
The C code is given in \cref{lst:add}.

\begin{floatlst}[style = cstyle]{add}{Adding a Q4.3 to a Q5.2 fixed-point number in C.}
int8_t a = 0x0d; // Q5.2 with decimal value 3.25
int8_t b = 0x25; // Q4.3 with decimal value 4.625
int8_t sum = (a << 1) + b; // sum will be Q4.3 with decimal value 7.875 
\end{floatlst}

Alternatively it is also possible to convert $b$ from Q4.3 to Q5.2 by shifting it one place to the right.
This operation can not cause an overflow but some precision is lost.
After the shift we can use an integer addition because both numbers are Q5.2 and the result will also be Q5.2 (when we assume that no overflow occurs when performing the integer addition).
The C code is given in \cref{lst:adda}.

\begin{floatlst}[style = cstyle]{adda}{Adding a Q4.3 to a Q5.2 fixed-point number in C. Please note the \ccode{sum} is less precise than the \ccode{sum} calculated in \cref{lst:add}.}
	int8_t a = 0x0d; // Q5.2 with decimal value 3.25
	int8_t b = 0x25; // Q4.3 with decimal value 4.625
	int8_t sum = a + (b >> 1); // sum will be Q5.2 with decimal value 7.75 
\end{floatlst}

Fixed-point numbers can be subtracted in a similar way. For example we can calculate $a-b$ by converting $a$ to Q4.3 and perform an integer subtraction.
The result will be Q4.3 in two's complement notation (when we assume that no overflow occurs when performing the integer subtraction). The operation is shown in \cref{fig:sub}. The result is $11110.101_{\text{two's complement}} = -00001.011_2 = -1.375$, which is the correct answer.

\begin{myFigure}
	\centering%
	\begin{BVerbatim}
	00011.010 -
	00100.101 =
	11110.101 
	\end{BVerbatim}
	\caption{Subtracting two Q4.3 fixed-point numbers.}
	\label{fig:sub}
\end{myFigure}   

\section{Multiply and Divide}
If we multiply two fixed-point numbers we can just multiply them by using integer multiplication.
The only thing left to do, is figuring out where the radix point must be placed in the result. 
\Cref{fig:mul} shows how $a \cdot{} b$ can be calculated. 

\begin{myFigure}
	\centering%
	\begin{BVerbatim}
	        000011.01 *
	        00100.101 =
	        000.01101 +
	       0000.0000  +
	      00001.101   +
	     000000.00    +
	    0000000.0     +
	   00001101       +
	  00000000        +
	 00000000         =
	 0000001111.00001
	\end{BVerbatim}
	\caption{Multiplying a Q5.2 with a Q4.3 fixed-point number.}
	\label{fig:mul}
\end{myFigure}   


As can be seen%
%Hack to prevent overfull hbox in non-ebook version.
\iftoggle{ebook}{ in \cref{fig:mul},}{,}
the result is a Q9.5 fixed-point number with value 0000001111.00001 ($0000001111.00001_2 = 15.03125_{10}$), which is the correct answer.
The precision if the result is higher than the precision of the multiplier and multiplicand. We can convert the result to the same precision as the multiplier (Q4.3) by shifting it two places to the right and truncating it to 8 bits. We obviously will lose some precision and before the truncation we must check if the result can be represented in 8 bits. This will yield a Q4.3 fixed-point number with value 01111000 ($01111.000_2 = 15_{10}$). 

In general, when we multiply a Q$n_1,m_1$ fixed-point number by a Q$n_2,m_2$ fixed-point number, the result will be a Q$n_1+n_2,m_1+m_2$ fixed-point number.

When we program in C it is important to prevent an overflow while calculating a product. Most of the time the multiplier and the multiplicand must be casted to a bigger data type before the multiplication is performed.

Divisions can be performed in a similar manner as multiplications. 
In general, when we divide a Q$n_1,m_1$ fixed-point number by a Q$n_2,m_2$ fixed-point number, the result will be a Q$n_1+m_2,m_1-m_2$ fixed-point number.
