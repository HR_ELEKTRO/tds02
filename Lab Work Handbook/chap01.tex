% !TeX spellcheck = en_US
\chapter{Introduction}
Digital Signal Processing (DSP) is an important aspect in the field of Embedded Systems Engineering.
For many years the huge interests and developments in the industry signify the importance of DSP techniques.
Important applications of DSP can be found in consumer electronics, e.g.\ media boxes, hearing aids, synthesizers, sound-cards and especially mobile phones.
In the industrial and research sector, DSP techniques are extensively used in motor and motion control, and in complex systems such as large sensor networks,
machine vision systems, telecommunication systems, control plants and satellite arrays for astronomical purposes (such as the LOFAR\footnote{\url{http://www.lofar.org/}.} in 2012).

The course is meant for final year students of the minor Embedded Systems.
In this course, a short introduction (or refreshment) to DSP theory will be given.
If you want more background information or detailed information we recommend the books \citetitle{Reay2015} \cite{Reay2015} and \citetitle{Kuo2013} \cite{Kuo2013}.

Electrical signals can be directly processed by analog components such as operational amplifier.
It is not possible to directly use DSP in an analog environment.
To enable DSP, the analog electrical input signals must first be sampled and digitalized by an Analog to Digital Converter (ADC).
After processing the digital output signal can be transfered back to the analog domain by using a Digital to Analog Converter (DAC).
A typical digital processing system, used in an analog environment, is shown in \cref{fig:dsp}.
In which $x(t)$ is the analog input signal which is a function of the time $t$, $y(t)$ is the analog output signal, $x[n]$ is the digital, discrete input signal indexed by the sample number $n$, and $y[n]$ is the digital, discrete output signal.

% specific tikz settings for block diagrams
\tikzset{%
  block/.style = {draw, thick, rectangle, minimum height = 3em, minimum width = 3em, inner sep=1em, font=\sffamily\Large},
}

\begin{myFigure}
    \centering%
%    \iftoggle{ebook}{\begin{adjustbox}{max width=\textwidth}}{}
    \begin{tikzpicture}[auto, thick, ->, >={Stealth[scale=1.5, inset=0.6pt]}, shorten >=1pt]
        \draw
            node (text) {$x(t)$} 
            node [coordinate, node distance=.5cm, right of=text] (input) {} 
            node [block, node distance=2cm, right of=input] (adc) {ADC}
            node [block, node distance=3.5cm, right of=adc] (dsp) {DSP}
            node [block, node distance=3.5cm, right of=dsp] (dac) {DAC}
            node [coordinate, node distance=2cm, right of=dac] (output) {}
            node [node distance=.5cm, right of=output] {$y(t)$}; 
        \draw (input) -- (adc);
        \draw (adc) -- node[pos=.35] {$x[n]$} (dsp);
        \draw (dsp) -- node[pos=.35] {$y[n]$} (dac);
        \draw (dac) -- (output);
    \end{tikzpicture}
%    \iftoggle{ebook}{\end{adjustbox}}{}
    \caption{Digital signal processing in an analog environment.}
    \label{fig:dsp}
\end{myFigure}


The advantages of DSP compared to analog signal processing are \cite{Kuo2013}:
\begin{itemize}
\item Flexibility.
The behavior of a DSP system is mainly determined by its software.
The behavior of analog systems, on the other hand, is entirely determined by its hardware.
This makes digital systems much easier to adapt to changing functional requirements or to enhance their performance.
\item Reliability.
The characteristics of analog components change when the environment (e.g.\ the temperature) changes and also deteriorate with age.
Therefore, the performance of analog signal processing systems will drift with changing environmental conditions and over time.
The performance of DSP systems will not drift.
\item Reproducibility.
Due to the tolerances of analog components two identically produced analog signal processing units will not have completely the same characteristics.
Therefore analog units often need fine-tuning before being taken into use.
Two identically produced and programmed DSP units will always have exactly the same characteristics so fine-tuning is not needed.
\item Complexity.
Using digital processing, complex applications which are not possible with analog techniques are feasible.
For example: face and speech recognition, data compression, MRI scanners, and radar tracking.
\item Costs.
Because many DSP systems can share the same hardware (the behavior is implemented in software), a DSP system almost always costs less than its analog counterpart.
\end{itemize}

The disadvantages of DSP compared to analog signal processing are:
\begin{itemize}
\item Bandwidth.
DSP systems have a limited bandwidth determined by the sample rate.
The bandwidth is limited to half of the sample frequency.
This limit is called the Nyquist frequency or folding frequency.
Analog signal processing systems have, in theory, unlimited bandwidth.
\item Precision.
DSP systems have a limited precision determined by the number of bits used for the ADC and DAC.
Analog signal processing systems have, in theory, unlimited precision.
\end{itemize}

An example of a simple algorithm that can be implemented in the DSP block shown in \cref{fig:dsp} is a so called Finite Impulse Response (FIR) filter.
The equation for a Finite Impulse Response (FIR) filter is:
\begin{equation}
\label{eq:fir}
\eqfir
\end{equation}
In which $y[n]$ represents the output sample with index $n$ and $x[n-k]$ stands for the input sample with index $n-k$.
The constant $N$ is the so called order of the filter.
The constants, so called coefficients of the filter, $b_k$ determine the characteristics of the filter.

As can be seen in \cref{eq:fir} the calculation of $y[n]$ uses $N+1$ multiplications and $N$ additions.
The accumulation of the results of multiplications is a frequently used operation in many DSP algorithms.
Also note that the calculation of $y[n]$ consist of a small loop.
Small loops frequently occur in DSP algorithms.
We will explore FIR filters and their implementation further in \csecref{sec:fir}.

There are two types of DSP applications: non-real-time and real-time.
For real-time systems the value of output sample $y[n]$ must be calculated before a certain deadline.
In most real-time DSP systems, the output samples must be produced at the same rate as the input signal is sampled.
Tools which run on a PC like MATLAB can be used for non-real-time DSP.
For real-time DSP we can use specific hardware (e.g.\ a digital signal processor). For relatively slow sample rates (e.g.\ audio applications) we can also use a modern generic processor. For example, in this course we will use an ARM\textsuperscript{®} Cortex\textsuperscript{®}-M4 MCU to implement our DSP (audio) algorithms.
As we will discover in \cref{MATLABfilterdesign}, MATLAB can also be used to design real-time DSP algorithms that will be executed on an embedded processor.

This course will consist mainly of working on practical assignments within the field of DSP.
A DSP application development board is available for the lab.
The goals of this course are mainly to teach the students to apply DSP algorithms in practice and to learn to work with the specialized hardware (i.e.\ a codec) that is available on the market today. The codec (coder-decoder) will be introduced in \cref{sec:codec}. 

\section{Purpose and Prerequisites}
The purpose of this course is to teach you to:
\begin{itemize}
\item work with several important components of a DSP system,
\item write simple C programs to implement a filter using an ARM processor and a codec,
\item design filters in MATLAB and use them with your own C code,
\item design, implement and test a FIR and IIR filter, and
\item optimize your C code and exploit the specific features of a modern codec.
\end{itemize}

The prerequisites of this course are:
\begin{itemize}
\item know how to program, and
\item know how to program microcontrollers.
\end{itemize}

\section{Course Planning}
The module Training Digital Signal Processing (TDS02) is awarded with 3 ECTS-credits.
Passing this module will take about 80 working hours which consist of: 
\begin{itemize}
\item 8 lab sessions of about 2.5 hours each = 20 hours total.
\item about 60 hours of homework (prepare, write code, use MATLAB, write reports) (about 5.5 hours per week).
\end{itemize}

\section{Document Organization}
This document consists of several parts:
\begin{itemize}
\item \cref{sec:preass} is an introduction to working with the course specific development boards.
\item \cref{sec:fir} will refresh your knowledge about FIR filters and windowing and introduce you to the MATLAB filter toolbox.
It also contains the first of the assignment that counts for your grade.
\item \cref{sec:iir} will refresh your knowledge about IIR filter.
It introduces several IIR filter structures that can be programmed.
This chapter also contains the second assignment that counts for your grade.
\item \cref{sec:optimizing} will teach you how to profile your code and how to take advantage of the specific features provided by a modern codec to speed up your code.
This chapter also contains the third, and last, assignment that counts for your grade.
\item \cref{sec:fixedpoint} introduces fixed-point arithmetic.
\end{itemize}
