% !TeX spellcheck = en_US
\chapter{FIR Filters}
\label{sec:fir}
In this chapter we will focus on designing and implementing a Finite Impulse Response (FIR) filter.
The formula for a FIR filter is:
\begin{equation}
\tag{\ref{eq:fir}}
\eqfir
\end{equation}
Where $y[n]$ are the output samples, $b_k$ are the filter coefficients, $x[n]$ are the input samples, and $N$ is the order of our filter.

\section{Determination of the Coefficients}
Usually we want to filter signals in the time domain, because signals are a function of time in the real world and not of frequency.
However, when we speak about filters we often define their response in the frequency domain.
We can use some math to transform our filter back to the time domain.

We will now show how this is done.
This is a summary of what you may have learned in the DSP01 course (see \cite[Chapter 5]{Lynn2004}).

The Inverse Discrete-Time Fourier Transform
(IDTFT) is given by:
\begin{equation}
x[n]=T_s\int_{\frac{-1}{2T_s}}^{\frac{1}{2T_s}}X(f)\cdot{}{e}^{j2\pi{}nfT_s}df
\end{equation}
Where $X(f)$ is the spectrum of our signal, $f$ is the frequency, and $T_s$ is the period of our discrete-time steps which is $\frac{1}{f_s}$, where $f_s$ is the sample frequency.

This is an integral transformation which transforms a signal in the frequency domain ($X(f)$) to the continuous-valued discrete-time domain ($x[n]$).

Since the Discrete-Time Fourier Transform (DTFT) is a linear transformation, we may say convoluting two signals in the time domain is the same as multiplying their spectra in the frequency domain.
Therefore we can easily design the frequency response in the frequency domain and then transform it back to the time domain so we can implement it in, for example, a microcontroller.

Let's look at the frequency
response magnitudes of a low-pass filter:
\begin{equation}
\left|H_{lp}(f)\right| = \begin{cases} 
1 & \dfrac{-{f}_{s}}{a}\;\leq{}\;f\;\leq{}\;\dfrac{{f}_{s}}{a},\ a\;\leq{}\;2 \\ 
0 & \text{otherwise}
\end{cases}
\end{equation}

Note that $a$ must be smaller than 2 because half the sample rate equals the Nyquist-Fre\-quen\-cy.
The function is shown in \cref{fig:lpffreqresp}.

% figuren staan in subdir figs
% #1 OPTIONAL = placement argument voor figure b.v. tb or H 
% #2 = width of scale optie voor \includegraphics
% #3 = filenaam (zonder extensie); labelnaam=fig:filenaam
% #4 = caption tekst
\figuur{}{lpffreqresp}{Low-pass filter frequency response.}

Also recall that the function is symmetric around the origin of the graph due to the complex conjugate properties of the Fourier Transform of some function.
We could also have drawn this figure from 0 to $\frac{f_s}{2}$, but this would make the integral we have to solve, for the inverse transform, a bit more cumbersome.

We can transform the above function of frequency $H(f)$ back to a function of time using the IDTFT.
Note that $f_c$ is the cut-off frequency of the filter, $a = \frac{f_s}{f_c}$.
So for example, if we have a sample rate of \SI{8000}{\hertz} and we want our cut-off frequency to be \SI{1000}{\hertz}, we take $a = 8$.

The IDTFT of a low-pass filter now becomes (substituting $T_s = \frac{1}{f_s}$ and changing the integral limits to confirm with the frequency response of the low-pass filter,
since only at that interval our function $H(f) = 1$ and it is $0$ anywhere else):
\begin{equation}
h_{lp}[n]=\frac{1}{f_s}\int_{\frac{-f_s}{a}}^{\frac{f_s}{a}}e^{j2\pi{}n\frac{f}{f_s}}df
\end{equation}
Then by solving the integral we find:
\begin{equation}
h_{lp}[n]=\frac{1}{f_{s}j2\pi{}n\frac{1}{f_s}}{\left[e^{j2\pi{}n\frac{f}{f_s}}\right]}_{\frac{-f_s}{a}}^{\frac{f_s}{a}}
\end{equation}
Substituting the limits gives:
\begin{equation}
h_{lp}[n]=\frac{1}{j2\pi{}n}\left[e^{\frac{j2\pi{}n}{a}}-{e}^{\frac{-j2\pi{}n}{a}}\right]
\end{equation}
By using Euler's Formula $e^{\alpha{}j}=\cos(\alpha{})+j\sin(\alpha)$ we find:
\begin{equation}
h_{lp}[n]=\frac{1}{j2\pi{}n}\left[\cos\left(\frac{2\pi{}n}{a}\right)+j\sin\left(\frac{2\pi{}n}{a}\right)-\cos\left(\frac{-{2\pi{}n}}{a}\right)-j\sin\left(\frac{-{2\mathit\pi{}n}}{a}\right)\right]
\end{equation}
Which simplifies to:
\begin{equation}
h_{lp}[n]=\frac{1}{j2\pi{}n}2j\sin\left(\frac{2\pi{}n}{a}\right)
\end{equation}
Further simplification gives:
\begin{equation}
h_{lp}[n]=\frac{\sin\left(\frac{2}{a}\pi{}n\right)}{\pi{}n}=\frac{2}{a}\text{sinc}\left(\frac{2n}{a}\right)
\end{equation}

The function $\text{sinc}(n) = \dfrac{\sin(n\pi{})}{n\pi{}}$ is called the normalized cardinal sine function and is widely used in DSP techniques.

We can now determine any $h_{lp}[n]$ with this formula, except for the non-trivial case of $n=0$, because dividing by zero is not possible.
We can determine the value of $h_{lp}[0]$ by calculating the limit of $h_{lp}[n]$ for $n\rightarrow{}0$.
Because the limit of $n\rightarrow{}0$ for both the nominator and denominator are zero, L'Hôpital's rule can be applied:
\begin{equation}
h_{lp}[0]
=\lim_{n\rightarrow{}0}{\frac{\sin\left(\frac{2}{a}\pi{}n\right)}{\pi{}n}}
=\lim_{n\rightarrow{}0}{\frac{\dfrac{d\left(\sin\left(\frac{2}{a}{\pi{}n}\right)\right)}{dn}}{\dfrac{d\left(\pi{}n\right)}{dn}}}
=\lim_{n\rightarrow{}0}{\frac{\frac{2}{a}\pi{}\cdot{}\cos\left(\frac{2}{a}\pi{}n\right)}{\pi{}}}
=\frac{2}{a} 
\end{equation}

In a similar way we can derive the responses of high-pass, band-pass and band-stop filters as well:

High-pass:
\begin{equation}
h_{hp}[n]=\frac{-\sin\left(\frac{2}{a}\pi{}n\right)}{\pi{}n}
\end{equation}
with $H_{hp}(f)$ = $1$ for $|f|\;\geq\;\frac{f_s}{a}$,
$0$ otherwise.

Band-pass:
\begin{equation}
h_{bp}[n]=\frac{\sin\left(\frac{2}{b}\pi{}n\right)-\sin\left(\frac{2}{a}\pi{}n\right)}{\pi{}n}
\end{equation}
with $H_{bp}(f)$ = $1$ for $\frac{f_s}{a}\;\leq\;|f|\;\leq\;\frac{f_s}{b}$,
$0$ otherwise.

Band-stop:
\begin{equation}
h_{bs}[n]=\frac{\sin\left(\frac{2}{a}\pi{}n\right)-\sin\left(\frac{2}{b}\pi{}n\right)}{\pi{}n}
\end{equation}
with $H_{bs}(f)$ = $0$ for $\frac{f_s}{a}\;\leq\;|f|\;\leq\;\frac{f_s}{b}$,
$1$ otherwise.

\section{Example}
For example, in some digital system with a sample frequency of \SI{8}{\kilo\hertz}, we might want to make a low-pass filter with pass-band \SIrange{0}{1000}{\hertz} and stop-band \SI{1000}{\hertz} to $F_s/2$ Hz.

For our cut-off frequency of \SI{1000}{\hertz}, first we calculate $a$, which is:
\begin{equation}
a=\frac{f_s}{f_c}=\frac{8000}{1000}=8
\end{equation}
Now we can calculate our first coefficient:
\begin{equation}
h_{lp}[0]=\frac{2}{a}=\frac{1}{4}=0.25
\end{equation}
Now for $n\neq{}0$:
\begin{equation}
h_{lp}[n]=\frac{\sin\left(\frac{\pi{}n}{4}\right)}{\pi{}n}
\end{equation}
The first 10 coefficients on both sides of $n=0$ of this filter are shown in \cref{tab:lpfcoef} and \cref{fig:lpfcoef}.

\begin{minipage}{\textwidth}
\begin{minipage}[m]{0.30\textwidth}
% commando voor tabel
% #1 OPTIONAL = placement argument for figure e.g. tb or H 
% #2 = caption tekst
% #3 = labelnaam=tab:#3
% #4 = tabu kolom definities
% #5 = kop tabel (eerste regel)
% #6 = inhoud tabel (rest van de regels)
\tabel[H]{Coefficients.}{lpfcoef}{rr}{$n$ & $h[n]$}{
0 & 0.250 \\
1, -1 & 0.225 \\
2, -2 & 0.159 \\
3, -3 & 0.075 \\
4, -4 & 0.000 \\
5, -5 & -0.045 \\
6, -6 & -0.053 \\
7, -7 & -0.032 \\
8, -8 & 0,000 \\
9, -9 & 0.025 \\
10, -10 & 0.032 
} 
%Hack: http://tex.stackexchange.com/questions/34971/how-to-keep-a-constant-baselineskip-when-using-minipages-or-parboxes
\par\xdef\tpd{\the\prevdepth}
\end{minipage}
\hfill
\begin{minipage}[m]{0.67\textwidth}
% commando voor figuur
% figuren staan in subdir figs
% #1 OPTIONAL = placement argument voor figure b.v. tb or H 
% #2 = width of scale optie voor \includegraphics
% #3 = filenaam (zonder extensie); labelnaam=fig:filenaam
% #4 = caption tekst
\figuur[H]{width=\textwidth}{lpfcoef}{Discrete-time transfer function of a low-pass filter.} 
\end{minipage}
\end{minipage}

\prevdepth\tpd\par
\vspace{.8\baselineskip}
Note that we should fill in any integer for $n$, not just -10 to 10.
If the number of coefficients gets very large, the delay will be very long.
Even so, if we wish to fully replicate the ideal filter response we've used in the previous example, we need to let $n$ go from $-\infty{}$ to $+\infty{}$.
This would give an infinite delay, so our ideally filtered signal will never appear on the output while the universe lasts, not to mention that we need infinite memory in our DSP system.

\section{Windowing}
Because we cannot allow a real filter to have an infinite number of coefficients, we will need to limit the response of our filter.
In the above case, where we cut off all $|n|\ge{}11$, we can see what result this has on the frequency response of our filter if we transform it back to the frequency domain (using the DTFT).
Because this is a lot of work, we will do this in MATLAB.
We can use the \mcode{freqz()} function in MATLAB to calculate the frequency response, see \proglink{lpfvbfreqresp.m}.
The result is shown in \cref{fig:lpfvbfreqresp}.

\figuur{width=.9\textwidth}{lpfvbfreqresp}{Frequency response of an abruptly ended transfer function.}

We can now see that our frequency response is not ideal anymore and that so-called side-lobes are introduced where the desired frequency response should be 0.
Also the steepness of the filter is not as good as in the ideal case since we have a limited number of coefficients.
This is mainly due to the abrupt ending of the desired discrete-time representation of our transfer function $h[n]$.

As you must recall from the DSP01 course we can let the coefficients slowly but more steadily approach to zero near the edges of our \enquote{window}
(the part of the filter we're interested in), by somehow scaling the coefficients a bit using a method called \emph{windowing}.
This way, the abrupt ending of coefficients (which results in the occurrence of, among other things, the side-lobes) will be somewhat compensated for.
This is at the cost of our filter to be less like the ideal filter.

Recall the formula for the output of a non-recursive filter:
\begin{equation}
\tag{\ref{eq:fir}}
\eqfir
\end{equation}

Note that since our transfer function $h[n]$ (and the window functions which we will discuss later) are non-causal (they depend on values of the future),
when implementing the filters, we shift the transfer function $h[n]$ backward in time so that each coefficient $b_k=h\left[k-\frac{N}{2}\right]$ (assuming that $N$ is even).
For more information, see \cite[page 151]{Lynn2004}.

We can expand this formula by taking the windowing function into account:
\begin{equation}
y[n]=\sum_{k=0}^{N}{w_k}\cdot{}{b_k}\cdot{}x[n-k]
\end{equation}
Where $w_k$ are the coefficients of our window.

For example, we can take a simple window called the Hamming window (recall DSP01).
The formula for a Hamming window is:
\begin{equation}
w[n]=0.54+0.46\cos{}\left(\frac{2\pi{}n}{N-1}\right)
\end{equation}
Where $N$ is the order of the filter.

The Hamming window is one of the most commonly used windows \cite{Kuo2013}.
The window itself is shown in \cref{fig:Hamming}.

\figuur{width=.9\textwidth}{Hamming}{The Hamming window.}

When we apply this window to our desired, but
abruptly ended discrete-time transfer function we get the discrete-time transfer function shown in \cref{fig:lpfHamming}.

\figuur{width=.9\textwidth}{lpfHamming}{Discrete-time transfer function adjusted by the Hamming window.}

Compare \cref{fig:lpfHamming} to \cref{fig:lpfcoef}.
Note that the coefficients and the edge of the function slowly decrease to 0,
therefore avoiding the abrupt ending of our transfer function, and thus reducing unwanted effects such as side-lobes, etc.
For comparison, see \cref{fig:lpfHammingfreqresp} which shows the frequency response of the filter with and without the application of the Hamming window.
The frequency response graph shown in red is that of our original filter with the abrupt ending of coefficients (also called a rectangular window),
and the graph shown in blue is that of our filter with the Hamming window applied.

\figuur{width=.9\textwidth}{lpfHammingfreqresp}{The effect of the application of the Hamming window on the frequency response (blue) compared to applying a rectangular window (red).}

Although the steepness of the filter is decreased, the side-lobes of our Hamming windowed filter are smaller
(note that the first side-lobe for the Hamming windowed filter has a maximum magnitude of -50 dB, and for the rectangular window this maximum magnitude is -20 dB).

There are many other window types, and the Hamming window is certainly not one of the best windows.
For your assignment you may use any other window type as long as you give arguments for choosing a certain type and discuss its properties in your report.

\section{MATLAB Filter Designer}
\label{MATLABfilterdesign}
We have already shown that it takes a lot of work to derive the coefficients, not to mention to analyse what happens with different windows.
We can use a special tool in MATLAB to do this easier and faster.
We will give an example of the same filter we specified above.

Start MATLAB and type in \mcode{filterDesigner} (previously known as FDA Tool).
The window shown in \cref{fig:fdatool} will open.

\figuur{width=.9\textwidth}{fdatool}{The Filter Designer main window.}

In MATLAB's Filter Designer we can design filters more easily than when we have to calculate all the values by hand.
We will give a short overview on the different sections of this window.

\begin{itemize}
\item \textbf{Current Filter Information:}\\
Here we can see what type of filter implementation we{\textquoteright}re aiming for (we will discuss these types later).
Also the order of the filter can be seen.
We can see whether the filter we{\textquoteright}ve designed is stable (always stable for FIR filters) as well.
For IIR filters, if you want to change the structure, you can right click on this section to change this option.
\item \textbf{Response Type:}\\
Here we can select the different response types.
We will only use low-pass, high-pass, band-pass and band-stop.
Also the design method can be selected.
For now we will use the FIR method with windowing, but note there are many other ways to design a filter.
In \cref{sec:IIR} we will design an IIR filter.
\item \textbf{Filter Order:}\\
Here we can specify the order, or we can specify to let MATLAB determine the minimum needed order of our filter based on the other specifications.
\item \textbf{Options:}\\
Here we can select different options and specify parameters for different filter types, windows types, etc.
\item \textbf{Frequency Specifications:}\\
Here we can specify the frequency properties of our filter, like the sample rate, pass frequencies and stop frequencies.
\item \textbf{Magnitude Specifications:}\\
Here we can select the magnitude properties of our filter, like the magnitudes in the pass- and stop-band.
\item \textbf{Filter Specifications:}\\
This section gives a graphical overview of the selected filter and the designed filter.
Basically this gives us the frequency response, but we can also show the phase response if we want.
\end{itemize}

Now use the tool to design a FIR low-pass filter of order 20, using a rectangular window.
The cut-off frequency should be \SI{1}{\kilo\hertz} and the sample rate should be \SI{8}{\kilo\hertz}.
Make sure to deselect the \enquote{Scale Passband} option in the Options section after you've selected the windowed method.
When you push the \enquote{Design Filter} you should see the same result as shown in \cref{fig:lpffdatool}.

\figuur{width=.9\textwidth}{lpffdatool}{\SI[detect-all]{1}{\kilo\hertz} low-pass filter in MATLAB's Filter Designer.}

Now we are interested in the coefficients of the filter.
In the menu, click \enquote{Analysis} and then \enquote{Filter Coefficients}.
Verify that they are the same as the coefficients we presented earlier in \cref{tab:lpfcoef}.
We can see the benefit of using a tool like MATLAB's Filter Designer now, since we won't have to calculate all the values by hand.

We can even export the coefficients to a C header that we can use in our programs.
In the menu, select \enquote{Targets}, \enquote{Generate C header\dots{}}.
Now a new dialog is shown, see \cref{fig:gencheader}.

\figuur{width=.52\textwidth}{gencheader}{Generate C header dialog.}  
% width = 454/786 * .9\textwidth to keep the same scale as other figures.

The numerator is the name your coefficients will get later on; the numerator length is a variable which represents the order of your filter + 1.
Because we use a Cortex-M4 without floating-point support to implement the filter, we export the coefficients as signed 16-bit integers.

If we open the exported file, we can see the filter coefficients at the bottom, and the variable representing the order + 1 of our filter.
Note that the order + 1 is the number of coefficients, and thus the size of our buffer which we will have to use to store delayed samples later on.

The lines that are actually useful are the last 6 ones, which define the variable named \ccode{BL} which is initialized with the number of coefficients%
\footnote{%
    Note that the number of coefficients is one more than the order of the filter.%
}  and the array named \ccode{B} which is initialized with the values of the coefficients.
Since we will not make use of the header file \code{tmwtypes.h} from MATLAB,
remove everything except those last lines and modify the code so the file \proglink{fdacoefs.h} looks like the one shown in \cref{lst:fdacoefs}.
The variable \ccode{BL} is replaced by a define so we can use this identifier to declare the size of the array \ccode{B}.
We have used the type \ccode{int16_t} to define the array \ccode{B}.
The type \ccode{int16_t} is defined in the standard C include file \ccode{stdint.h}\footnote{%
	\ccode{stdint.h} is a header file in the C standard library introduced in the C99 standard library to allow programmers to write more portable code by providing a set of typedefs that specify exact-width integer types. See: \url{http://en.cppreference.com/w/c/types/integer}.
}.

% #1 = (optional) extra key=value pairs for lstinputlisting e.g.
%      style = cstyle, cppstyle, mstyle (C, C++, MATLAB)
%      style = linenumbers
%      linerange={1-6, 13-27}
% #2 = filename (must be located in the subdirectory progs)
% #3 = label=lst:#3
% #4 = caption
\floatlstinput[style=cstyle]{fdacoefs.h}{fdacoefs}{16-bit signed integer coefficients generated by MATLAB.}

The \ccode{const} keyword actually means that we cannot change the values stored in the array \ccode{B} during run-time.

Note that our coefficients have been scaled now from floating-point numbers in MATLAB, to signed two's complement fixed-point integers.
The 16-bit numbers generated by MATLAB have 15 fractional bits and one sign bit.
This fixed-point format is often referred to as Q0.15.

For example, the center coefficient \ccode{B[10]} is equal to 8192.
This value is obtained by multiplying the floating point value 0.25 by the largest signed 16-bit value + 1 which is $2^{15}$.

To make the filter causal, the coefficients have been moved by order/2 samples to the right, since we cannot grab samples in the future.
Now the delay is a bit longer but the filter output over a longer time will be the same.
As before we define the coefficients $b_k=h\left[k-\frac{N}{2}\right]$ (assuming that $N$ is even) for $k=0\dots{}N$.
So now coefficient $b_0$ (or \ccode{B[0]} in the C code) is equal to $h[-10]$.
The center coefficient $h[0]$ is now referred to as \ccode{B[10]} in the C code.

Now we know how we can calculate the coefficients, a next assignment is given.

\section{Assignment 5: Finite Impulse Response Filter}
\label{sec:firfilter}
%Het woord buffer wordt in mathmode niet netjes weergegeven
%Zie: http://tex.stackexchange.com/questions/6819/ugly-spacing-around-f-in-math-mode
\newcommand{\buffervar}{bu\hspace{-0.05em}f\hspace{-0.2em}f\hspace{-0.15em}er}
\begin{myFigure}
	\centering%
	\tikzstyle{startend} = [rectangle, rounded corners, minimum width=2cm, minimum height=.75cm, text centered, draw=black, text = white, fill=hrred]
	\tikzstyle{io} = [inner sep = 1pt,trapezium, trapezium stretches body = true, trapezium left angle=70, trapezium right angle=110, minimum width=4cm, minimum height=1cm, text centered, draw=black]
	\tikzstyle{process} = [rectangle, minimum width=4cm, minimum height=1cm, text centered, text width=4cm, draw=black]
	\tikzstyle{decision} = [shape aspect = 2,diamond, minimum width=4cm, minimum height=1cm, text centered, draw=black]
	\tikzstyle{arrow} = [thick,->,>=stealth']
	\scalebox{.8}{%
	\begin{tikzpicture}[node distance=1.5cm]
	\node (1) [startend] {start};
	\node (2) [io, below of = 1] {$sample$};
	\node (3) [process, below of = 2] {$\buffervar{}[0] = sample$};
	\node (4) [process, below of = 3] {$output = 0$\\$k = 0$};
	\node (5) [process, below of = 4] {$output = output + B[k] \times{} \buffervar{}[k]$};
	\node (6) [process, below of = 5] {increase $k$};
	\node (7) [decision, below of = 6, yshift=-1mm] {$k \leq{} N$ ?};
	\node (8) [process, below of = 7, yshift=-3mm] {$i = N$};
	\node (9) [process, below of = 8] {$\buffervar{}[i] = \buffervar{}[i - 1]$};
	\node (10) [process, below of = 9] {decrease $i$};
	\node (11)  [decision, below of = 10, yshift=-1mm] {$i \geq{} 1$ ?};
	\node (12) [io, below of = 11, yshift=-3mm] {$output$};
	\node (13) [startend, below of = 12] {end};
	\draw [arrow] (1) -- (2);
	\draw [arrow] (2) -- (3);
	\draw [arrow] (3) -- (4);
	\draw [arrow] (4) -- (5);
	\draw [arrow] (5) -- (6);
	\draw [arrow] (6) -- (7);
	\draw [arrow] (7) -- node [anchor = west] {no} (8);
	\draw [arrow] (7) -- node [anchor = south east] {yes} ++(4,0) |- (5);
	\draw [arrow] (8) -- (9);
	\draw [arrow] (9) -- (10);
	\draw [arrow] (10) -- (11);
	\draw [arrow] (11) -- node [anchor = west] {no} (12);
	\draw [arrow] (11) -- node [anchor = south east] {yes} ++(4,0) |- (9);
	\draw [arrow] (12) -- (13);
	\end{tikzpicture}%
	}
    \caption{Flow diagram for the calculation of a new FIR filter output sample.}
\label{fig:firflow}
\end{myFigure}

In \cref{fig:firflow} we can see the flow diagram to implement the calculation of a new FIR filter output sample.
What is done, basically, is a buffer of size \ccode{BL} is filled with a new sample,
where \ccode{BL} is the number of coefficients and the size of our buffer (so the order of the filter $N$ is \ccode{BL-1}).
Now, the new sample is calculated by using the formula for a FIR filter:
\begin{equation}
\tag{\ref{eq:fir}}
\eqfir
\end{equation}
Note that $N$ is the order of the filter here.
Suppose we have a filter of order $N = 2$, then our current output $y[n]$ is:
\begin{equation}
y[n]={b}_{0}\cdot{}x[n]+{b}_{1}\cdot{}x[n-1]+{b}_{2}\cdot{}x[n-2]
\end{equation}
We change this so that the current sample $x[n]$ is always stored in \ccode{buffer[0]},
and mirroring time because we will implement this in C, and we cannot use negative offsets in arrays to store previous values.
So a delayed sample in the buffer of time $n-5$,
will be stored in \ccode{buffer[5]} instead of \ccode{buffer[-5]}.
So to calculate the current output $y[n]$, called \ccode{output} in the C code, we may now write:

\begin{lstfragment}[style=cstyle]
    output = B[0]*buffer[0] + B[1]*buffer[1] + B[2]*buffer[2];
\end{lstfragment}

This is what is done in \cref{fig:firflow} as well.
Note that \ccode{buffer[k]} in the C code corresponds to $x[n-k]$ in \cref{eq:fir} and that \ccode{B[k]} corresponds to the coefficient $b_k$ in \cref{eq:fir}.

After we've calculated the new output sample, we shift all the entries in the buffer to create the delay on each sample.
This is also shown in \cref{fig:firflow}.
When we're done, we can send the output sample to the codec.

Here is your assignment:
Implement a C program that executes a \SI{1}{\kilo\hertz} LP FIR Filter with a sample rate of \SI{8}{\kilo\hertz}.
Use the coefficients from \cref{lst:fdacoefs}.

After the implementation is complete, put different frequencies on the input and verify the output to correspond with the designed filter in MATLAB using an oscilloscope. You are advised to use the Soundcard Oscilloscope program\footnote{%
	\url{https://www.zeitnitz.eu/scms/scope_en}
} on a PC to generate a frequency response graph. 
The best result is obtained by using a frequency sweep input signal generated by a signal generator. 

%TODO screenshot invoegen.
Show the result to your instructor.
\handtekening[a]

In the last loop of the flowchart shown in \cref{fig:firflow} each sample in the buffer is moved one place towards the end of the buffer.
By using a circular buffer we can make the filter implementation somewhat faster.
If we use a circular buffer we just keep track of the position of the oldest sample in the buffer and override this value with the new input sample at the start of the flowchart.
Now, change your program to use a circular buffer%
\footnote{%
    More information about circular buffers can be found at \url{https://en.wikipedia.org/wiki/Circular_buffer}.%
} and show the result to your instructor.
Note that you also have to change the code in the first loop shown in \cref{fig:firflow} because the most recent sample is no longer located at index 0 in the buffer.
\handtekening[b]

When your code works, you will get a new filter specification from your instructor.
Create new coefficients using MATLAB's Filter Designer yourself and write a report about this assignment.
%TODO: should be put in an appendix
The \href{https://bitbucket.org/HR_ELEKTRO/tds02/wiki/Report\%20Requirements/Report_Requirements_TDS02\iftoggle{ebook}{_ebook}{}.pdf}{guidelines for the report} can be found in the course wiki.

When your new filter is implemented and has the proper characteristics, show the result to your instructor.
\handtekening[c]
