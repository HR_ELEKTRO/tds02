% !TeX spellcheck = en_US
\chapter{Preliminary Assignments}
\label{sec:preass}
\begin{clstShortInline}

This chapter provides an introduction to working with the course specific DSP development boards and software development environment.
It contains:
\begin{itemize}
\item An introduction to the most important components of the DSP development boards: ARM\textsuperscript{®} Cortex\textsuperscript{®}-M4 MCU processor and the coder-decoder (codec).
\item A tutorial about working with the software development environment: Code Composer Studio.
\item An assignment to generate an output signal with the DSP development boards.
\item An assignment to capture an input signal with the DSP development boards.
\item An assignment to recall some DSP basics and teach you how to create a time delay by using a buffer.
\end{itemize}

\iftoggle{ebook}{%
\section {Assignment 0: Introduction to the \audioboard{} and \launchpad{} Boards}
}{%
\section {Assignment 0: Introduction to the \launchpad{} and \audioboard{} Boards}
\markright{Assignment 0: Introduction to the Boards}
}
This lab work handbook uses the \audioboard{} Audio BoosterPack\footnote{See: \url{http://www.ti.com/tool/CC3200AUDBOOST}.} \cite{CC3200AUDBOOST}, shown in \cref{fig:CC3200AUDBOOST}, in combination with the CC3220S LaunchPad development board\footnote{See: \url{http://www.ti.com/tool/cc3220s-launchxl}.} \cite{CC3220LAUNCHXL}, shown in \cref{fig:CC3220S-LAUNCHXL}. 
\figuur{width=.5\textwidth}{CC3200AUDBOOST}{The \audioboard{} Audio BoosterPack.}

\figuur{width=.5\textwidth}{CC3220S-LAUNCHXL}{The \launchpad{} SimpleLink™ Wi-Fi\textsuperscript{®} LaunchPad™ Development Kit.}

The two most important components on these boards are the CC3220S Simple\-Link™ Wi-Fi\textsuperscript{®} Wireless Microcontroller Unit and the \codec{} Codec (co\-der-de\-coder). 
The CC3220S System-on-Chip (SoC) \cite{ProcessorDatasheet, ProcessorReference} is a single-chip with two separate execution environments: an user application dedicated ARM\textsuperscript{®} Cortex\textsuperscript{®}-M4 MCU and a network processor MCU. 
The \codec{} \cite{CodecDatasheet, CodecReference} is a 20-bit stereo audio codec with embedded miniDSP which can operate with a sample rate of up to 192 ksps.

\subsection{\codec{} Codec}
\label{sec:codec}
The two most important components within a codec are the Analog to Digital Converter (ADC) and the Digital to Analog Converters (DAC).
As can be seen in \cref{fig:codec} the \codec{} stereo codec includes not only two ADCs and two DACs but also includes several amplifiers and signal processing blocks.

\figuur{width=\textwidth}{codec}{Simplified block diagram of the codec \citepdfpag{CodecReference}{3}.}

As can be seen in the schematics of the \audioboard{} \citepdfpag{Schematics}{1} the stereo LINE IN input of the board are connected to the IN1L and IN1R inputs of the codec, and the HPL (HeadPhone Left) and HPR outputs of the codec are connected to the stereo LINE OUT output of the board.

The codec is connected to the CC3220 Launchpad through two serial buses: an $\text{I}^{2}\text{C}$ bus%
\footnote{%
    $\text{I}^{2}\text{C}$ stands for Inter-Integrated Circuit, more information can be found at:
    \url{https://en.wikipedia.org/wiki/I\%C2\%B2C}.%
} and an $\text{I}^{2}\text{S}$ bus%
\footnote{%
    $\text{I}^{2}\text{S}$ stands for Inter-IC Sound, more information can be found at:
    \url{https://en.wikipedia.org/wiki/I\%C2\%B2S}.%
}.
The $\text{I}^{2}\text{C}$ bus is used to configure and control the codec and the $\text{I}^{2}\text{S}$ bus is used to transfer the audio samples.

The codec contains an analog programmable gain amplifier before the ADC and a digital volume control after the DAC.
Despite its name, this digital volume control is implemented as an analog amplifier with programmable gain.
The amplification factors are specified in decibels (dB), as can be seen in \cref{fig:codec}.
This is a logarithmic unit which is frequently used in electrical engineering.
The gain in dB ($G_{\text{dB}}$) of an amplifier can be calculated as follows:
\begin{equation}
G_{\text{dB}} = 20\cdot{}\log_{10}{\frac{V_{out}}{V_{in}}}
\end{equation}
In which $V_{in}$ and $V_{out}$ are the input respectively the output voltages of the amplifier.

Using a logarithmic scale for the amplification of audio signals makes sense because the sensitivity of the human ear to sound pressure works on a logarithmic scale too.

Besides the ADCs, DACs, and amplifiers the codec also contains:
\begin{itemize}
\item Two miniDSP cores.
The first miniDSP core is tightly coupled to the ADC, the second miniDSP core is tightly coupled to the DAC.
They support application-specific algorithms in the record and playback paths of the device. The miniDSP cores are fully software controlled. 
Target algorithms, like active noise cancellation, acoustic echo cancellation or advanced DSP filtering can be loaded into the device after power-up.
\item ADC and DAC signal-processing blocks for filtering and effects.
These processing blocks support different types of digital filtering.
\item Automatic Gain Control (AGC).
AGC can be used to maintain a nominally-constant output level.
\item Dynamic Range Compression (DRC).
DRC automatically adjusts the gain of the DAC to prevent
hard clipping of peak signals.
\item Beep generator.
This generator can generate a sine wave signal.
\item Digital Auto Mute.
This feature switches of the output signal when the input is constant.
This eliminates high-frequency noise during silent periods of music or speech.
\item Headset Detection.
The codec can determine which type of headset is plugged in.
\end{itemize}

The codec is a complicated digital signal processing component on its own and it's documentation \cite{CodecDatasheet, CodecReference} can be quite overwhelming at first.

\subsection{CC3220S SoC}
\label{sec:CC3220S}
The functional block diagram of the CC3220S SimpleLink™ Wi-Fi\textsuperscript{®} Wireless and Internet-of-Things Solution, a Single-Chip Wireless MCU, is shown in \cref{fig:CC3220S}.

\figuur{width=.5\textwidth}{CC3220S}{Functional block diagram of the CC3220S SoC \citepdfpag{ProcessorDatasheet}{4}.}

The CC3220S System-on-Chip (SoC) is a single-chip with two separate execution environments: an user application dedicated ARM\textsuperscript{®} Cortex\textsuperscript{®}-M4 MCU and a network processor MCU.

The ARM Cortex-M4 has no hardware support for floating-point calculations. Therefore, fixed-point calculations will be used to implement the DSP algorithms discussed in this \documenttype{}.
Floating-point numbers use a constant number of significant bits (the significant) which are scaled by an exponent.
The decimal number 1234.56789 can be encoded in decimal floating-point notation as $1.23456789 \times 10^3$ and also as $123456789 \times 10^{-5}$.
As you can see the position of the decimal point can \enquote{float} within the number by adjusting the value of the exponent.
In computing systems the IEEE754 standard \cite{IEEE754} to represent real numbers is almost always used.
This standard defines several formats for example single precision (which is used to implement the type \ccode{float} in the C programming language) and
double precision (which is used to implement the type \ccode{double} in C).
Double precision numbers in the IEEE754 standard are 64 bits wide.
One bit is used to determine the sign, 52 bits are used for the significant and 11 bits are used for the exponent%
\footnote{%
    See: \url{https://en.wikipedia.org/wiki/Double-precision_floating-point_format}.%
}.
The floating-point representation makes it possible to cover a wide, dynamic range of values with a constant number of significant bits.
A double precision number has 52 significant bits which corresponds to about 16 significant decimal digits.

Fixed-point numbers use a fixed number of digits after and before the radix point.
For example 12 bits before and 4 bits after the binary point.
The format of a fixed-point binary number can be specified by using the Q$n.m$ notation.
In which $n$ is the number of bits before the binary point (without the sign bit) and $m$ is the number of bits after the binary point.
For example, a number in Q0.15 format has one sign bit, zero bits before the binary point (a zero is implied) and 15 bits after the binary point.
If the number of bits before the binary point is zero the Q format is sometimes abbreviated by omitting the $n$.
For example Q0.15 can be abbreviated as Q15.
Fixed-point numbers can be used to represent a limited range of values with a constant resolution.
There is no direct support (build-in types) for fixed-point numbers in the C programming language.
MATLAB, on the other hand, does support fixed point numbers.
In \cref{sec:fixedpoint} a small introduction into fixed-point arithmetic is given.

The disadvantage of using floating-point numbers is that a significant amount of hardware is needed to perform fast floating-point calculations.
This hardware uses a significant amount of power.
Fixed-point calculations, on the other hand, only need about the same amount of hardware as integer calculations do.
Therefore, in embedded systems where price and or power usage must be minimized, fixed-point numbers are often preferred over floating-point numbers.

The ARM Cortex-M4 CPU has several specific features which enables it to execute digital signal algorithms fast \cite{CortexM4DSP}:
\begin{itemize}
\item 
As mentioned in the introduction, DSP algorithms frequently use multiply-addition combinations.
For example, in a FIR filter implementation input samples are multiplied by coefficients and added together.
The Cortex-M4 has specific MAC (Multiply ACcumulate) instructions. For example, it is capable of a 16-bit x 16-bit multiplication and a 32-bit add in a single cycle.
\item 
The Cortex-M4 also has a 16-bit SIMD vector processing unit.
With this SIMD (Single Instruction Multiple Data) unit the Cortex-M4 can execute four 8-bit or two 16-bit calculations with only one instruction.
\item In a general purpose CPU an overflow occurs when the result of an arithmetic operation on two signed numbers overflows the sign bit.
For example, when the largest possible 16-bit signed value ($2^{15}-1 = 32767 = \text{0x7FFF}$)%
\footnote{%
    The prefix 0x is used to denote hexadecimal notation.%
} is incremented by one the result is $\text{0x8000} = -32768 = -2^{15}$.
In a general purpose CPU this overflow is signaled by a flag in some status register, but it is the responsibility of the programmer to take appropriate actions.
The Cortex-M4 has instructions which will signal an overflow but it also has an alternative set of instructions which are called saturating instructions.
When these instructions are used, the output of a calculation is clipped to its maximum or minimum value when the sign bit overflows.
For example, when the largest possible 16-bit signed value ($32767 = \text{0x7FFF}$) is incremented by one in a saturating addition instruction the result is $\text{0x7FFF} = 32767$.
In many DSP algorithms saturation is the proper thing to do when the sign bit threatens to overflow.
When this is the case the programmer can use the saturating instructions and does not has to check for overflows any more.
\end{itemize}

Besides these DSP specific features the Cortex-M4 also has features which will speed up the execution of generic algorithms such as pipelining and branch prediction \cite{CortexM4}.

To execute DSP algorithms even more efficiently Texas Instruments also provides processors which are specialized for this task; so called DSP's (Digital Signal Processors).
The C5000 family\footnote{%
	\url{http://www.ti.com/processors/dsp/c5000-dsp/overview.html}
} of DSPs is optimized for fixed-point calculations and is very energy efficient.
They also offers a family of DSPs which are optimized for floating-point calculations: the C6000 family\footnote{%
	\url{http://www.ti.com/processors/dsp/c6000-dsp/overview.html}
}.
Other companies which produce DSPs are: Analog Devices\footnote{%
	\url{http://www.analog.com/en/products/processors-dsp/dsp.html}
}, NXP Semiconductors\footnote{%
	\url{https://www.nxp.com/products/processors-and-microcontrollers/additional-processors-and-mcus/digital-signal-processors:Digital-Signal-Processors}
}, and others\footnote{%
	\url{https://en.wikipedia.org/wiki/Digital\_signal\_processor\#Modern\_DSPs}
}.

%TODO: Add Assignment 0: Review Questions.
\subsection{Electrostatic Discharge}
Before we continue there is one very important thing to know:
\textbf{\textcolor{hrred}{The \launchpad{} and \audioboard{} development boards are sensitive to electrostatic discharge (ESD)!}}

\figuur{width=.4\textwidth}{esd}{The \launchpad{} and \audioboard{} are sensitive to ESD.}

Before you actually touch the board, observe the following precautions:
\begin{itemize}
\item Ground yourself by using a wrist-strap.
\item Always use a shielded bag if you need to transport the board.
\end{itemize}

If you fail to comply with these precautions you can damage the board beyond repair.

\section{Assignment 1: Working with Code Composer Studio}
In this assignment you will run and test a demo program on your \launchpad{} and \audioboard{} development boards.
You need a source to produce an audio signal (preferable a signal generator) and a way to inspect the output (preferable an oscilloscope).
Alternatively you can use your smartphone and a headset to test the demo program.

\subsection{Installing Software and Configure Hardware}
The website \url{http://tds02.bitbucket.io/} explains how to:
\begin{itemize}
	\item 
	install the software needed for this course:
	\begin{itemize}
		\item 
		Tera Term.
		A terminal emulator is needed because a lot of TI's demo programs use a (virtual) terminal connection to report statuses and errors. 
		Tera Term was chosen because it recognizes which (virtual) serial ports are available. 
		\item
		UniFlash.
		This program is used to program the flash memory on the \launchpad{} board.
		\item
		Soundcard Oscilloscope.
		In the lab you can use a normal oscilloscope but at home you can use this program to generate and measure signals. You can also use this this program to generate frequency response graphs.
		\item
		Code Composer Studio (CCS).
		This Integrated Development Environment (IDE) will be used to develop software for the CC3220S. This IDE is provided for free by Texas Instruments and is based on the Eclipse open source IDE which is widely used.
		\item
		SimpleLink Software Development Kit (SDK).
		This SDK contains software libraries which make software development for the CC3220S more easy. It is also contains many example projects.
	\end{itemize}
	\item 
	reconfigure the \launchpad{} to not start a Wi-Fi access point when the board is powered up.
	\item
	recompile the driverlib (a part of the SimpleLink SDK).
	To properly debug programs which use the driverlib, it must be recompiled.
	\item
	connect the \audioboard{} Audio BoosterPack to the \launchpad{} board.
\end{itemize} 

\subsection{Running the Demo Program}
\label{sec:demoprogram}
Follow the description given at: \url{http://tds02.bitbucket.io/} to run the demo program on your \launchpad{} and \audioboard{} boards.

Code Composer Studio is an Eclipse-based IDE.
All Eclipse-base IDEs work from a certain workspace.
In this tutorial the workspace directory \hcode{C:\\workspace\_v9\\\\-CC3220S} is used.

\textbf{\textcolor{hrred}{Please note that after completing an assignment or at the end of the lab,
you have to move your complete workspace from the local \code{C:\\} drive to your private network drive so that you will
not lose your work when other students use the computer!}} 

Be sure to move, and not copy, your workspace to prevent others from using your work without your consent.
Whenever you start with the lab again, you can just copy back your workspace to \code{C:\\}.
You may also place your workspace on your private network drive \code{H:\\} and let CCS work from there, but this might slow down the compilation process.

It is recommended to play a little bit with the demo program in the debug perspective.
Try to add breakpoints and variables watches.
If you think everything is working fine, call your instructor.
\handtekening{}

\section{Assignment 2: Generating Output}
For this assignment you need an oscilloscope to view the signal that is generated by the \audioboard{} board.
If you do not have a oscilloscope available then you can use your PC using the program which can be found here: \url{https://www.zeitnitz.eu/scms/scope_en}.
%The microphone input of your PC is probably to sensitive so you have to attenuate the signal.

\subsection{Polling-based Output}
The most straightforward method for sending samples to the codec is to use polling which is explained in this section.

Open Code Composer Studio and copy the demo project \code{line_in_2_line_out} to a new project called \code{audioSine1kHz}.
Replace the code in the file \code{main_nortos.c} with the code from \proglink{audioSine1kHz/main_nortos.c} shown in \cref{lst:audiosine}.

% this listing is longer than one page therefore we can not use a float
\lstinput[
    linerange={6-76},
	firstnumber=6,
    style=cstyle,
    style=linenumbers
]{audioSine1kHz/main_nortos.c}{audiosine}{Program to generate a \SI{1}{\kilo\hertz} sine wave on the left audio output channel.}

Connect the LINE OUT output of the \audioboard{} board to the oscilloscope, compile and run the program, and view the output signals on the scope.

The signal is quite noisy.
You should select \enquote{HF Rejection} in the trigger menu to properly trigger and measure the signal.
First press the \enquote{Autoset} button on the scope, then press the \enquote{MENU} button in the \enquote{TRIGGER} section of the scope.
First select \enquote{Slope/Coupling} and then press \enquote{Rejection Off} twice to select \enquote{Rejection HF}.
With this setting the scope will reject the high frequency noise in the signal when triggering.
Your scope should display something similar to \cref{fig:scopeonekhz}.
The colors are inverted in this figure to save some ink.

\figuur{scale=1}{scopeonekhz}{\SI{1}{\kilo\hertz} sine wave.}

Samples are transfered from the CC3220S chip to the \codec{} codec by using the I\textsuperscript{2}S bus\footnote{%
	More information about the I\textsuperscript{2}S bus can be found at \url{https://en.wikipedia.org/wiki/I\%C2\%B2S}%
}.

Every time the function |I2SDataPut| \citepdfpag{ProcessorReference}{382} is called, it will stay in some loop that waits for the codec to be ready to accept new data.
The codec expects two new samples every sample time $T_s$. One sample for the left audio channel and one sample for the right audio channel. In this case the sample frequency $f_s$ is \SI{48}{\kilo\hertz} so the sample time $T_s = 1/f_s$ is \SI{20.833}{\micro\second}. So the function |I2SDataPut| must be called every \SI{10.417}{\micro\second}.
If there is any spare time between two calls to the function |I2SDataPut|, it keeps waiting until a new sample must be sent.
When the codec is ready to receive a new sample the function |I2SDataPut| will actually send the sample.
When the function |I2SDataPut| is called too late, a so called underflow error is generated by the I\textsuperscript{2}S hardware and the communication with the codec comes to a halt. 

The program in \cref{lst:audiosine} repeatedly waits (inside the function |I2SDataPut|) until a new sample can be written to the codec.
The implementation of the |I2SDataPut| can be found in the driverlib\footnote{%
	\code{C:\\ti\\simplelink\_cc32xx\_sdk\_3\_20\_00\_06\\source\\ti\\devices\\cc32xx\\driverlib\\i2s.h}
}. 
The function repeatedly checks the XDATA bit in the XSTAT register \citepdfpag{ProcessorReference}{423} to check is the I\textsuperscript{2}S controller is ready to send data.
This repeatedly checking is called \enquote{polling}.
If our program has nothing else to do, polling is fine.
But if we want our program to perform some other actions we must be very careful to provide the samples on time because when we do not provide a new sample on time the signal communication with the codec will come to a halt.
Before we look into a different method to output samples, change the program to generate a square wave of \SI{1}{\kilo\hertz} on the right audio channel while keeping the 1 kHZ sine on the left audio channel.
The top-top amplitude of the square wave should be equal to the top-top amplitude of the sine wave.

Change the sampling frequency to \SI{8}{\kilo\hertz} and explain to your instructor what happens.
\handtekening[a]

\subsection{Interrupt-based Output}
\label{sec:intbasedoutput}
Another way to output (and input) samples is interrupt-based.
This way, our processor will (instead of constantly waiting for the codec to demand a sample) jump to a dedicated piece of code called an interrupt routine,
whenever the codec indicates that it wants a new sample.
This has advantages over polling.

Here is a nice analogy.
Suppose that you're following a lecture on DSP and, between every sentence, your lecturer will ask you and all of your class-mates:
\enquote{Do you have a question?} 
Your lecturer is now working polling-based, spending a lot of time and effort in \enquote{polling} the students.

Instead of doing this, an agreement can be made that if the students have a question,
they raise their hand so they can \enquote{interrupt} the lecturer to ask a question (after the lecturer finishes the current sentence).
Then, the lecturer only has to answer when a question arises.
Now, the lecturer is working interrupt-based, which is obviously much more efficient,
since now the lecturer can keep talking when there are no questions.

This is somewhat the same for the processor and the codec.
When the codec has a question \enquote{Can I get a new sample?},
the processor finishes its current instruction, and then only has to give some sample to the codec in a brief moment.
Then the processor can go back to its original task at hand.

To facilitate interrupt handling, we'll make use of the driverlib \citepdfpag{ProcessorReference}{383}.
In \cref{lst:audioInterrupt} a simple interrupt-based program is given.
This program can be downloaded from \proglink{audio\\-Interrupt/\\-main_nortos.c}.

% #1 = (optional) extra key=value pairs for lstinputlisting e.g.
%      style = cstyle, cppstyle, mstyle (C, C++, MATLAB)
%      style = linenumbers
%      linerange={1-6, 13-27}
% #2 = filename (must be located in the subdirectory progs)
% #3 = label=lst:#3
% #4 = caption
\lstinput[
    style=cstyle,
    style=linenumbers,
    firstnumber=6,
    linerange=6-82, 
]{audioInterrupt/main_nortos.c}{audioInterrupt}{A simple interrupt-based program.}

As you can see on line 79 the |main| function of this program simply burns clock cycles in a |while (1)|-loop after initializing the codec and the interrupt.
In this case, it is not useful to use an interrupt but in a real world application the |main| function can perform other tasks without worrying about the \enquote{feeding} of the codec.
Normally, within this while loop there will be function calls to do all kinds of things the application has to do (for example communicate with some network device, or read data from storage).
However, calculating and sending a new output sample now happens in the interrupt service routine (ISR) called |I2S_ISR()|.
Whenever the codec needs a sample, it will interrupt the processor.
The processor will jump into the ISR, send data to the codec, calculate a new sample, and continue with the normal program.
Keep in mind that interrupt service routines should be as small and quick as possible and should not contain any polling themselves,
hence it might defeat the whole purpose of an interrupt.

The variables |n| and |data| which are defined inside the ISR, see \cref{lst:audioInterrupt} line 33 to 38, are declared by using the |static| keyword.
If we hadn't done this, these variables would have been freshly created each time the ISR is called.
By declaring these variables as static local variables%
\footnote{%
    For more information about static local variables see: \url{https://en.wikipedia.org/wiki/Static_variable}.%
} their lifetime is extended to the time the program ends.
Although, their scope is still local to the function in which they are declared.
    
On line 14 of \cref{lst:audioInterrupt} the file \code{i2s.h} is included.
This file declares the functions we can use to initialize the interrupt vector table and to enable the interrupts.
The call to the function |I2SIntRegister| \citepdfpag{ProcessorReference}{383} on line 75 defines the function to be called when the I\textsuperscript{2}S interrupt
occurs.
The call to |I2SIntEnable| on line 77 enables the \code{I2S_INT_XDATA} I\textsuperscript{2}S interrupt source.

Now, first predict the output on the left and right LINE OUT channels and then verify your predictions with the oscilloscope.
Explain the output signals to your instructor.
\handtekening[b]

The code shown in \cref{lst:audioInterrupt} can be used as a base program for all the other interrupt-based programs you will write during this course.

Write and test an interrupt-based program that outputs a sine on the left channel and a cosine on the right channel.
Use the XY function of the oscilloscope to display a circle.

Make use of a the sine look-up table used in \cref{lst:audiosine}.
Note that you do not need a separate cosine look-up table.
The sine and cosine should have a frequency of \SI{1}{\kilo\hertz} and the sample rate should be \SI{48}{\kilo\hertz}.
The amplitudes should be as high as possible.

Show the result to your instructor.
\handtekening[c]

\section{Assignment 3: Receiving Input}
In \csecref{sec:demoprogram} you already tested the demo project called \code{line_in_2_line_out} which simply copies the signal from the LINE IN input to the LINE OUT output.
This demo program uses polling-based input.

\subsection{Interrupt-based Input}
Copy the project \code{line_in_2_line_out} to a new project called \hcode{line_in_2_line\\-_out_in\\-ter\\-rupt} and modify the program to work interrupt-based.
Use a sampling rate of \SI{48}{\kilo\hertz}.

You can find the names of the individual I\textsuperscript{2}S interrupt sources in Table 12-1 of the \citetitle{ProcessorReference} \citepdfpag{ProcessorReference}{385}.

When your program is working, use a signal generator to apply a \SI{1}{\kilo\hertz} saw-tooth shaped signal with an amplitude of \SI{1}{V\textsubscript{pp}}
to the left channel of the LINE IN input and verify that the signal on the LINE OUT output is similar.
What is the delay between the input and output signals?   

Show your program and the result to your instructor.
\handtekening{}

\subsection{Audio Input}
Connect an audio output (e.g. your smartphone) to the LINE IN input.
Verify that you hear the audio signal from the input on the output by connecting headphones to the LINE OUT output.

\section{Assignment 4: Delays}
\label{sec:delays}
When making filters, we will need a buffer.
Recall the formulas of the preliminary assignment.

One nice application of a buffer is an echo effect.
For this assignment we will create this effect on the \launchpad{} and \audioboard{} boards.
You should bring a headset to test it.

Suppose we have some circular buffer with |N| entries |buffer[N]|.
A circular buffer is a buffer that, if we want to fill the buffer at some time |n|, we fill it at index |buffer[n mod N]|.
This is a formal way of saying that we just let some variable count up with 1 for every sample which indicates the buffer location,
and when the variable reaches the end of the buffer, we just reset that variable to 0 so it \enquote{circulates} around the buffer from the end back to the start.

%Het woord buffer wordt in mathmode niet netjes weergegeven
%Zie: http://tex.stackexchange.com/questions/6819/ugly-spacing-around-f-in-math-mode
\newcommand{\buffervar}{bu\hspace{-0.05em}f\hspace{-0.2em}f\hspace{-0.15em}er}
\begin{myFigure}
	\centering%
	\tikzstyle{startend} = [rectangle, rounded corners, minimum width=2cm, minimum height=.75cm, text centered, draw=black, text = white, fill=hrred]
	\tikzstyle{io} = [inner sep = 1pt,trapezium, trapezium stretches body = true, trapezium left angle=70, trapezium right angle=110, minimum width=4cm, minimum height=1cm, text centered, draw=black]
	\tikzstyle{process} = [rectangle, minimum width=4cm, minimum height=1cm, text centered, text width=4cm, draw=black]
	\tikzstyle{decision} = [shape aspect = 2,diamond, minimum width=4cm, minimum height=1cm, text centered, draw=black]
	\tikzstyle{arrow} = [thick,->,>=stealth']
	\scalebox{.8}{%
		\begin{tikzpicture}[node distance=1.5cm]
		\node (1) [startend] {start};
		\node (2) [process, below of = 1] {$n = 0$\\ fill $\buffervar{}$ with zeros};
		\node (3) [io, below of = 2] {$input\ sample$};
		\node (4) [process, below of = 3, yshift=-2.5mm] {$output\ sample= input\ sample + \buffervar{}[n] \times{} c$};
		\node (5) [io, below of = 4, yshift=-2.5mm] {$output\ sample$};
		\node (6) [process, below of = 5] {$\buffervar{}[n] = output\ sample$};
		\node (7) [process, below of = 6] {$n = (n + 1) \mod{} N$};
		\draw [arrow] (1) -- (2);
		\draw [arrow] (2) -- (3);
		\draw [arrow] (3) -- (4);
		\draw [arrow] (4) -- (5);
		\draw [arrow] (5) -- (6);
		\draw [arrow] (6) -- (7);
		\draw [arrow] (7) -- ++(0,-1) --+(4,0) |- (3);
		\end{tikzpicture}%
	}
	\caption{Flow diagram to create a simple echo effect.}
	\label{fig:echoflow}
\end{myFigure}

We can use this to create a nice echo effect, see \cref{fig:echoflow}
At time |n| we want to output the buffer value |buffer[n]| multiplied by some constant |c|, plus our current input sample.
After we output this sample, we put the sample in buffer location |buffer[n]|.
Now if |c == 1|, there will be an infinite echo which will make the signal louder and louder (don't try this, your ears will get hurt).
If we make |c| smaller than 1, say, 0.75, then every time the buffer index passes that entry again,
that original sample will become smaller (exponentially over time), and every time a new sample is added to it (which in turn becomes smaller every time after that as well).

Write and test a program that applies an echo effect on the audio input.
Use a sample frequency of \SI{48}{\kilo\hertz} and choose |N| so that the first echo will appear after \SI{0.5}{\second}.
Choose |c| to be 0.5 to start with.
Also set |c| to 0.75 and observe the difference.
The echo effect is best observed by using an audio fragment of spoken text.

Show the result to your instructor.
\handtekening{}

\end{clstShortInline}
