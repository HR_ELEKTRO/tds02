% !TeX spellcheck = en_US
\chapter{IIR Filters}
\label{sec:iir}
In this chapter we will focus on designing and implementing an Infinite Impulse Response (IIR) filter.
These filters are also called recursive filters.
A FIR filter, which was introduced in \cref{sec:fir}, uses a certain number of preceding input samples to calculate the current output sample.
An IIR filter not only uses preceding input samples, but it also uses a certain number of previous output samples.
Thus, the formula for an IIR filter is:
\newcommand{\eqiir}{
y[n]=\sum _{k=0}^{N}{{b}_{k}\cdot{}x[n-k]}-\sum
_{i=1}^{M}{{a}_{i}{\cdot}y[n-i]}
}
\begin{equation}
\label{eq:iir}
\eqiir
\end{equation}
Or:
\begin{multline}
y[n]=-a_1\cdot{}y[n-1]-a_2\cdot{}y[n-2]-\cdots{}-a_M\cdot{}y[n-M]+\\
b_0\cdot{}x[n]+b_1\cdot{}x[n-1]+\cdots{}+b_N\cdot{}x[n-N]
\end{multline}

The output of an IIR filter depends not only on the current and past inputs,
but also on the previous outputs (hence it is recursive).

\section{Determination of the Coefficients}
Digital recursive filters (which we often specify in the $z$-domain) are
relatively young compared to analogue recursive filters (which we often
specify in the $s$-domain).
Formulas for analogue filters are well
known to designers.
These formulas often form the basis for digital recursive
filter design as well, since there are methods to transform 
a formula in the $s$-domain into a formula in the $z$-domain.
Using such a method, the properties of the filter in the time-domain and frequency-domain are, approximately, preserved.
One popular method is called the Bilinear Transform (BLT).
We will give a short recap on the BLT (as seen in DSP01) and show a
simple example.

The BLT is:
\begin{equation}
s\approx{}\frac{2}{T_s}\cdot{}\frac{z-1}{z+1}
\end{equation}
Where $T_s$ is the sample period which is $\frac{1}{f_s}$ where $f_s$ is the sample frequency.

Also, recall that the frequency response of a continuous-time filter can be determined by evaluating the transfer function $H(s)$ at:
\begin{equation}
s=j\omega{}_c
\end{equation}
Likewise, the frequency response of a discrete-time filter can be determined by evaluating the transfer function $H(z)$ at:
\begin{equation}
z=e^{j\omega{}_d}
\end{equation}
Where $\omega{}_c$ are the continuous-time domain
frequencies and $\omega{}_d$ are the
discrete-time domain frequencies.

Since the $s$-domain analyses a system for $t\rightarrow\infty$, but the $z$-domain only concerns periodic signals, it is interesting to see the relation between $\omega{}_c$ and $\omega{}_d$.

We can find this by using the BLT:
\begin{equation}
j\omega{}_c\approx\frac{2}{T_s}\cdot{}\frac{e^{j\omega{}_d}-1}{e^{j\omega{}_d}+1}
\end{equation}
This simplifies to:
\newcommand{\eqwarp}{
    \omega{}_{c}\approx\frac{2}{T_s}\tan\left(\frac{\omega{}_{d}T_s}{2}\right)
}
\begin{equation}
\label{eq:warp}
\eqwarp
\end{equation}
See \cite[page 203]{Lynn2004} or \url{https://en.wikipedia.org/wiki/Bilinear_transform#Frequency_warping}.

Note that this is not a linear relation.
If
you design a filter in the $s$-domain and convert it to a filter in the
$z$-domain, especially at the edges of the spectrum of the discrete-time
filter, the response will be
\enquote{warped}.
This is due to the $\tan$
function defined in the relation.
This effect is called
frequency warping.
Before you apply the Z-transform,
any frequencies used in the $s$-domain transfer functions should
therefore first be \enquote{pre-warped}.

Now we have all the tools needed to transform a function of $s$ into a function of $z$.

\section{Example of a Simple Recursive Low-Pass Filter}
Suppose we want to make an IIR filter with sample rate of \SI{8000}{\hertz} similar to a low-pass RC filter with a cut-off frequency of \SI{1000}{\hertz}.
Recall the transfer function of a simple low-pass RC filter:
\begin{equation}
\label{eq:hslpf}
H(s)=\frac{1}{1+\dfrac{s}{\omega{}_{c}}}=\frac{\omega{}_{c}}{\omega{}_{c}+s}=\frac{\omega{}_{c}}{s+\omega{}_{c}}
\end{equation}

We can create this filter in MATLAB using: \mcode{hs =
tf(1000*2*pi, [1 1000*2*pi])} were \mcode{tf} stands for transfer function.
We can plot its frequency and phase
response using the \mcode{bodeplot} command as has been done in the program \proglink{lpf\\-bode\\-log.m}.
The result of this program is shown in \cref{fig:lpfbodelog}.

\figuur{width=.9\textwidth}{lpfbodelog}{Bode plot of a low-pass filter with a cut-off frequency of \SI{1000}{\hertz} drawn with a logarithmic frequency scale.}

The filter shown in \cref{fig:lpfbodelog} is clearly a low-pass filter with a cut-off frequency of \SI{1000}{\hertz}.
As you may know\footnote{See: \url{https://en.wikipedia.org/wiki/Low-pass_filter}} the amplification at the cut-off frequency should be $20\log(1/\sqrt{2})\approx-3.01\ \text{dB}$ and the phase at the cut-off frequency should be \SI{-45}{\degree}.
Both facts can be verified in \cref{fig:lpfbodelog}.

Note that the frequency is plotted with a logarithmic scale as is custom for Bode plots.
Also note that the frequency magnitude response of this filter will go to minus infinity as the frequency goes to infinity.

Because we are interested in the frequencies from \SIrange{0}{8000}{\hertz} the Bode plot is drawn again in \cref{fig:lpfbodelog} for this frequency range with the program \proglink{lpf\\-bode\\-lin.m}.
This time a linear frequency scale is used.

\figuur{width=.9\textwidth}{lpfbodelin}{Bode plot of a low-pass filter with a cut-off frequency of \SI{1000}{\hertz} drawn with a linear frequency scale.}

Now we can apply the BLT to the analog version of the transfer function to make it discrete.

Again:
\begin{equation}
\tag{\ref{eq:hslpf}}
H(s)=\frac{\omega{}_{c}}{s+\omega{}_{c}}
\end{equation}
Applying the BLT:
\begin{equation}
\begin{split}
H(z)&\approx{}\frac{\omega{}_{c}}{\dfrac{2}{T_s}\cdot{}\dfrac{z-1}{z+1}+\omega{}_{c}}
=\dfrac{\omega{}_{c}T_s(z+1)}{2(z-1)+\omega{}_{c}T_s(z+1)}
=\frac{\omega{}_{c}T_sz+\omega{}_{c}T_s}{2z-2+\omega{}_{c}T_sz+\omega{}_{c}T_s} \\
&=\frac{\omega{}_{c}T_sz+\omega{}_{c}T_s}{\left(\omega{}_{c}T_s+2\right)z+\omega{}_{c}T_s-2}
=\frac{\dfrac{\omega{}_{c}T_s}{\omega{}_{c}T_s+2}\cdot{}z+\dfrac{\omega{}_{c}T_s}{\omega{}_{c}T_s+2}}{z+\dfrac{\omega{}_{c}T_s-2}{\omega{}_{c}T_s+2}}
\end{split}
\end{equation}
By taking $\omega{}_{c}=1000\cdot{}2\pi$ and 
$T_s=\dfrac{1}{8000}$ we find:
\begin{equation}
H(z)\approx{}\frac{0.2820\,z+0.2820}{z-0.4361}
\end{equation}

We can also do this in MATLAB quickly:
\begin{lstfragment}[style=mstyle]
>> hs = tf(1000*2*pi, [1 1000*2*pi])

hs =
    6283
  --------
  s + 6283

Continuous-time transfer function.

>> hz = c2d(hs, 1/8000, 'tustin')

hz =
  0.282 z + 0.282
  ---------------
    z - 0.4361
 
Sample time: 0.000125 seconds
Discrete-time transfer function.
\end{lstfragment}

Now we can verify if the responses are the same using the program \proglink{lpf\\-zbode\\-lin.m}.
The result is shown in \cref{fig:lpfzbodelin}.
Note that the magnitude scale is extremely large (\SI{-400}{\deci\bel} represents an attenuation of $10^{20}$).

\figuur{width=.9\textwidth}{lpfzbodelin}{Bode plot of a discrete-time low-pass filter with a cut-off frequency of \SI{1000}{\hertz} and a sample frequency of \SI{8000}{\hertz}.}

We can see several interesting properties of the BLT transform in \cref{fig:lpfzbodelin}.
Due to the frequency warping, the whole frequency response of the analogue filter, for which the frequency goes to infinity,
is now \enquote{compressed} into the frequency response of the digital filter for which the response only goes to $f_{s}/2$.
The non-linear characteristic of the tangent is seen here.
Also, due to this warping effect, the cut-off frequency has somewhat shifted.
As we can see, the amplification at the intended cut-off frequency is \SI{-3.25}{\deci\bel} but should be \SI{-3.01}{\deci\bel}.
The phase at the intended cut-off frequency is \SI{-46.6}{\degree} but should be \SI{-45}{\degree}.
If we want to correct this, we have to apply the pre-warping.

We know that:
\begin{equation}
\tag{\ref{eq:warp}}
\eqwarp
\end{equation}
Now, if our desired cut-off frequency in the discrete-time version of our filter $\omega{}_{d}=2\pi \cdot{}1000$,
considering our sample rate of \SI{8000}{\hertz}, then in the analogue domain, the pre-warped frequency is:
\begin{equation}
\omega{}_{{c}_{prewarped}}\approx\frac{2}{\dfrac{1}{8000}}\tan\left(\frac{2\pi
\cdot{}1000\cdot{}\dfrac{1}{8000}}{2}\right)=16000\tan\left(\pi\cdot\frac{1000}{8000}\right)\approx{}6627\ \text{rad/s}
\end{equation}
If we use this pre-warped frequency in our derived formula for the low-pass filter, we find:
\begin{equation}
H(z)\approx\frac{
    \dfrac{\omega{}_{{c}_{prewarped}}T_s}{\omega{}_{{c}_{prewarped}}T_s+2}\cdot{}z+
    \dfrac{\omega{}_{{c}_{prewarped}}T_s}{\omega{}_{{c}_{prewarped}}T_s+2}
}{
    z+\dfrac{\omega{}_{{c}_{prewarped}}T_s-2}{\omega{}_{{c}_{prewarped}}T_s+2}
}\approx{}\frac{0.2929\cdot{}z+0.2929}{z-0.4142}
\end{equation}

We can do this in MATLAB even quicker:
\begin{lstfragment}[style=mstyle]
>> hs = tf(1000*2*pi, [1 1000*2*pi])

hs =
    6283
  --------
  s + 6283
 
Continuous-time transfer function.

>> hz = c2d(hs, 1/8000, 'prewarp', 1000*2*pi)

hz =
  0.2929 z + 0.2929
  -----------------
     z - 0.4142
 
Sample time: 0.000125 seconds
Discrete-time transfer function.
\end{lstfragment}

Now if we look at the Bode plot, shown in \cref{fig:lpfzwbodelin}, we see that the new filter has the correct amplitude and phase response at the cut-off frequency.

\figuur{width=.9\textwidth}{lpfzwbodelin}{Bode plot of a discrete-time low-pass filter with a cut-off frequency of \SI{1000}{\hertz} and a sample frequency of \SI{8000}{\hertz}.}

In \cref{fig:lpfszwbodelin} the Bode plot of the analog ($H(s)$) and digital ($H(z)$) filters are shown.

\figuur{width=.9\textwidth}{lpfszwbodelin}{Bode plots for $H(s)$ and $H(z)$.}

Now that we have the right response, we can easily calculate the coefficients for the filter.
Since:
\begin{equation}
H(z)=\frac{Y(z)}{X(z)}\approx\frac{\dfrac{\omega{}_{c_{prewarped}}T_s}{\omega{}_{c_{prewarped}}T_s+2}\cdot{}z+\dfrac{\omega{}_{c_{prewarped}}T_s}{\omega{}_{c_{prewarped}}T_s+2}}{z+\dfrac{\omega{}_{c_{prewarped}}T_s-2}{\omega{}_{c_{prewarped}}T_s+2}}
\end{equation}
We can now solve $Y(z)$.

For readability we substitute $u=\dfrac{\omega{}_{c_{prewarped}}T_s-2}{\omega{}_{c_{prewarped}}T_s+2}$ and $w=\dfrac{\omega{}_{c_{prewarped}}T_s}{\omega{}_{c_{prewarped}}T_s+2}$.
\begin{equation}
Y(z)\cdot{}z+u\cdot{}Y(z)=w\cdot{}X(z)\cdot{}z+w\cdot{}X(z)
\end{equation}
Now we make the filter causal by multiplying both sides with ${z}^{-1}$:
\begin{equation}
Y(z)+u\cdot{}Y(z)\cdot{}{z}^{-1}=w\cdot{}X(z)+w\cdot{}X(z)\cdot{}{z}^{-1}
\end{equation}
Thus:
\begin{equation}
Y(z)=-u\cdot{}Y(z)\cdot{}{z}^{-1}+w\cdot{}X(z)+w\cdot{}X(z)\cdot{}{z}^{-1}
\end{equation}
Now we can convert this to the time domain:
\begin{equation}
y[n]=-u\cdot{}y[n-1]+w\cdot{}x[n]+w\cdot{}x[n-1]
\end{equation}
If we substitute back $u$ and $w$:
\begin{equation}
y[n]=\frac{-{\omega{}_{c_{prewarped}}T_s-2}}{\omega{}_{c_{prewarped}}T_s+2}\cdot{}y[n-1]+\frac{\omega{}_{c_{prewarped}}T_s}{\omega{}_{c_{prewarped}}T_s+2}\cdot{}x[n]+\frac{\omega{}_{c_{prewarped}}T_s}{\omega{}_{c_{prewarped}}T_s+2}\cdot{}x[n-1]
\end{equation}
We find the coefficients for an implementable version of the recursive filter.

The design method described so far comes in handy when we want to design a filter dynamically in software.
For example when we do not now the cut-off frequency which is required beforehand.
If we now the requirements of the filter beforehand it is much easier to use the MATLAB Filter Design and Analysis tool.

\section{MATLAB's Filter Designer}
Calculating the recursive filter coefficients by hand takes a long time.
Therefore we will use MATLAB's Filter Designer to calculate the coefficients for our Infinite Impulse Response (IIR) filters.

IIR filters can have many different implementations.
Most implementations (or structures) can be selected in the Filter Designer.
Your assignment will be to implement one of these structures.

\section{Filter Structures}
The simplest form is the \emph{Direct-Form I Single Sections} structure.
If you right click in the \enquote{Current Filter Information} section of the Filter Designer design window, you can convert the structure of the filter, as can be seen in \cref{fig:fdastructure}.
By default, the structure for an IIR filter is: \enquote{Direct-Form II, Second Order Sections}.

\figuur{width=.9\textwidth}{fdastructure}{Options provided in the \enquote{Current Filter Information} section of the Filter Designer design window.}

The structures of the filter can be explored if you click Show Filter Structure.
Now, create a simple filter, convert its structure to \enquote{Direct-Form I}, and convert it to \enquote{Single Section}.
Select \enquote{Show Filter Structure} from the pop-up menu shown in \cref{fig:fdastructure} and
verify that the structure which is shown, which is duplicated in \cref{fig:directform1}, directly implements the formula for a recursive filter:
\begin{equation}
\tag{\ref{eq:iir}}
\eqiir
\end{equation}

\figuur{width=.9\textwidth}{directform1}{Direct-form I single section filter structure.}

Note that even though MATLAB will generate coefficient $a_1$,
this coefficient is often 1, and signifies the total output gain of the filter.
The direct-form I structure is the most straightforward implementation.
Also note that MATLAB begins the index of an array with 1, and not with 0 as in C.

Another filter structure is the direct-form II structure shown in \cref{fig:directform2}.

\figuur{width=.9\textwidth}{directform2}{Direct-form II single section filter structure.}

For this structure, the recursive and non-recursive part of the filter is swapped.
This has the advantage that the delay elements can be combined.
For more information see \cite{Enden2002}.

A disadvantage of these structures is that if there is only a small (e.g.\ rounding) error in any of the coefficients the output value will be incorrect due to the recursive nature of these filters.
Every (e.g.\ rounding) error will be recursively applied to the newly calculated samples.

Usually the higher coefficients have a smaller value than the lower coefficients.
If this is all stored in, for example, a 16-bit fixed-point number, then the very small values have less significant bits, a lower precision, and thus the round-off error is relatively big.
Therefore, with a limited amount of bits, the effective range of a coefficient value is low.
Even with floating point numbers this range will decrease exponentially (you might want to look up how floating-point numbers are stored%
\footnote{%
    \url{https://en.wikipedia.org/wiki/Floating_point}%
}).

A good solution to this is to \emph{cascade} the filters in smaller, second-order (or sometimes called biquadratic) sections.
This can be applied to both the direct-form I and direct-form II structures.
\Cref{fig:directform2cas} shows an example of an IIR second-order cascaded structure.

\figuur{width=.9\textwidth}{directform2cas}{Direct-form II cascaded filter structure.}

Note that the input of the section that is shown here is the output of a previous section that looks exactly the same (namely like a second order IIR filter), only with different coefficients.
Again, the output of this section is the input for the next section until there are no more sections for the sample to pass through.
By cascading multiple second-order filters, each filter coefficient will use most of the range of a digitally stored number, thus making round-off errors less pronounced in the final output.

Now that we've seen how the coefficients are calculated and how we can structure IIR filters, a new assignment is given.

Use MATLAB to calculate the filter coefficients.
Don't attempt to derive the coefficients with the BLT yourself.
This is outside the scope of the course, only mention the properties of the original analog equivalent of your filter in your report.

\section{Assignment 6: Infinite Impulse Response Filter}
\label{sec:IIR}
Choose an IIR structure to implement.
This can be:
\begin{itemize}
	\item direct-form I, or
	\item direct-form II.
\end{itemize}

The first part of the assignment is to implement a simple second order low-pass filter to test the code for your chosen structure.
Design a simple low-pass IIR filter in MATLAB with a cut-off frequency of \SI{1000}{\hertz} and a sample frequency of \SI{8000}{\hertz}.
It is important that you first test your code implementation before continuing with higher-order filters or cascaded structures.

Hint: First draw a flow diagram like the one in the FIR filter assignment, see \cref{fig:firflow}, before you write your code.

Developing an IIR filter using MATLAB's Filter Designer proves to be more difficult than expected. See these notes: \url{http://tds02.bitbucket.io/IIRfilter.htm}.

Show the result to your instructor.
\handtekening[a]

When your code works, you will get a new filter specification from your instructor.

You are free to choose between an implementation with a cascaded structure (of second order sections) or a single section filter.

Show the result of your new filter to your instructor.
\handtekening[b]

Write a report about this assignment.
The \href{https://bitbucket.org/HR_ELEKTRO/tds02/wiki/Report\%20Requirements/Report_Requirements_TDS02\iftoggle{ebook}{_ebook}{}.pdf}{guidelines for the report} can be found in the course repository.