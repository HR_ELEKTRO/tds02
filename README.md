# Training Digital Signal Processing #

In dit repository zijn de Modulewijzer en het Lab Work Handbook opgenomen die bij de module TDS02 (Training Digital Signal Processing) van de minor Embedded Systems van de Hogeschool Rotterdam gebruikt worden.

Je kunt de gecompileerde versies (in pdf) vinden in de [wiki](https://bitbucket.org/HR_ELEKTRO/tds02/wiki) van dit repository.

Op- en aanmerkingen zijn altijd welkom. Maak een [issue](https://bitbucket.org/HR_ELEKTRO/tds02/issues?status=new&status=open) aan of stuur een mail naar [Harry Broeders](mailto:j.z.m.broeders@hr.nl).